package fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializerBase64Impl;

import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializer.ImageSerializer;


import java.io.*;
import java.util.*;

public class ImageSerializerBase64Impl implements ImageSerializer {
    @Override
    public String serialize(File image){

        // on déclare un StringBuilder plutôt qu'une String,
        // au cas juste ou l'on aurait plusieurs opérations à faire
        StringBuilder base64Image = new StringBuilder();
        // on essaie d'accèder au flux de données renvoyé par notre fichier image
        try (FileInputStream imageInFile = new FileInputStream(image)) {
            // Si on y parvient on déclare un tableau d'octets de la taille de l'image
            byte[] imageData = new byte[(int) image.length()];
            // L'image est alors lu puis stoker dans le tableau d'octets
            imageInFile.read(imageData);
            // Puis en respectant l'encodage en base64, on ajoute ce tableau d'octets à notre stringBuilder de départ.
            base64Image.append(Base64.getEncoder().encodeToString(imageData));
        } catch (FileNotFoundException e) {
            System.out.println("Image manquante" + e);
        } catch (IOException ioe) {
            System.out.println("Exception levée à la lecture de l'image " + ioe);
        }
        // retour de notre StringBuilder sous forme d'une String, l'image est normalement encoder dans une chaine de caractères
        return base64Image.toString();
    }

    @Override
    public byte[] deserialize(String encodedImage) {

        // On a notre chaine de caractères représentant l'image encodée en base64
        // On retourne la conversion en un tableau d'octets
        return Base64.getDecoder().decode(encodedImage);
    }
}
